module "s3_endpoint" {
    source = "./modules/vpc"  
}


resource "aws_s3_bucket" "sagemaker_s3_bucket" {
    bucket = "sagemaker-files-s3-company" 
}

resource "aws_s3_bucket_ownership_controls" "sagemaker_user_control" {
  bucket = aws_s3_bucket.sagemaker_s3_bucket.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "example" {
  depends_on = [aws_s3_bucket_ownership_controls.sagemaker_user_control]

  bucket = aws_s3_bucket.sagemaker_s3_bucket.id
  acl    = "private"
}



# resource "aws_s3_bucket_policy" "BucketPolicy" {
#   bucket = "sagemaker-files-s3-company"
#   policy = <<POLICY
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Principal": "*",
#       "Action": "s3:*",
#       "Resource": [
#         "arn:aws:s3:::/*",
#         "arn:aws:s3:::"
#       ],
#       "Effect": "Deny",
#       "Condition": {
#         "StringNotEquals": {
#           "aws:sourceVpce": module.s3_endpoint.sagemaker_s3_endpoint
#         }
#       }
#     }
#   ]
# }
# POLICY
# }