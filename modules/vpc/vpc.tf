resource "aws_vpc" "sagemaker_vpc" {
    cidr_block = "10.0.0.0/16"
}

resource "aws_subnet" "sagemaker_private_subnet" {
    vpc_id = aws_vpc.sagemaker_vpc.id
    cidr_block = "10.0.0.0/24" 
}


resource "aws_subnet" "sagemaker_private_subnet_one" {
    vpc_id = aws_vpc.sagemaker_vpc.id
    cidr_block = "10.0.1.0/24" 
  
}


resource "aws_route_table" "sagemaker_private_rt" {
    vpc_id = aws_vpc.sagemaker_vpc.id
    route = []      
}

resource "aws_route_table_association" "sagemaker_private_rt_assosiation" {
    subnet_id = aws_subnet.sagemaker_private_subnet.id
    route_table_id = aws_route_table.sagemaker_private_rt.id  
}

resource "aws_route_table_association" "sagemaker_private_rt_one_assosiation" {
    subnet_id = aws_subnet.sagemaker_private_subnet_one.id
    route_table_id = aws_route_table.sagemaker_private_rt.id  
}


resource "aws_security_group" "sagemaker_sg" {
  name        = "Sagemaker-instance0Security-Group"
  description = "Allow HTTPS and HTTP inbound traffic"
  vpc_id      = aws_vpc.sagemaker_vpc.id

  ingress {
    description      = "HTTPS from VPC"
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.sagemaker_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

   ingress {
    description      = "HTTP from VPC"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [aws_vpc.sagemaker_vpc.cidr_block]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]

}
}

resource "aws_vpc_endpoint" "sagemaker_s3_endpoint" {
    vpc_id = aws_vpc.sagemaker_vpc.id
    service_name = "com.amazonaws.ap-south-1.s3"  
    vpc_endpoint_type = "Interface"
}

resource "aws_vpc_endpoint_subnet_association" "sagemaker_s3_endpoint_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_s3_endpoint.id
    subnet_id = aws_subnet.sagemaker_private_subnet.id
  
}

resource "aws_vpc_endpoint_security_group_association" "sagemaker_s3_endpoint_sg_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_s3_endpoint.id
    security_group_id = aws_security_group.sagemaker_sg.id 
}


resource "aws_vpc_endpoint" "sagemaker_cwl_endpoint" {
    vpc_id = aws_vpc.sagemaker_vpc.id
    service_name = "com.amazonaws.ap-south-1.logs" 
    vpc_endpoint_type = "Interface" 
}

resource "aws_vpc_endpoint_subnet_association" "sagemaker_cwl_endpoint_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_cwl_endpoint.id
    subnet_id = aws_subnet.sagemaker_private_subnet.id
      
}

resource "aws_vpc_endpoint_security_group_association" "sagemaker_cwl_endpoint_sg_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_cwl_endpoint.id
    security_group_id = aws_security_group.sagemaker_sg.id 
}


resource "aws_vpc_endpoint" "sagemaker_runtime_endpoint" {
  vpc_id      = aws_vpc.sagemaker_vpc.id
  service_name = "com.amazonaws.ap-south-1.sagemaker.runtime"
  vpc_endpoint_type = "Interface"
}

resource "aws_vpc_endpoint_subnet_association" "sagemaker_runtime_endpoint_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_runtime_endpoint.id
    subnet_id = aws_subnet.sagemaker_private_subnet.id
  
}

resource "aws_vpc_endpoint_security_group_association" "sagemaker_runtime_endpoint_sg_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_runtime_endpoint.id
    security_group_id = aws_security_group.sagemaker_sg.id 
}


resource "aws_vpc_endpoint" "sagemaker_api_endpoint" {
  vpc_id      = aws_vpc.sagemaker_vpc.id
  service_name = "com.amazonaws.ap-south-1.sagemaker.api"
  vpc_endpoint_type = "Interface"
}

resource "aws_vpc_endpoint_subnet_association" "sagemaker_api_endpoint_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_api_endpoint.id
    subnet_id = aws_subnet.sagemaker_private_subnet.id
  
}

resource "aws_vpc_endpoint_security_group_association" "sagemaker_api_endpoint_sg_assosiation" {
    vpc_endpoint_id = aws_vpc_endpoint.sagemaker_api_endpoint.id
    security_group_id = aws_security_group.sagemaker_sg.id 
}



