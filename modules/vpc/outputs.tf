output "sagemaker_vpc_id" {
    value =  aws_vpc.sagemaker_vpc.id  
}

output "sagemaker_subnet_id" {
    value = [aws_subnet.sagemaker_private_subnet.id, aws_subnet.sagemaker_private_subnet_one.id]  
}

output "sagemaker_securitygrp" {
    value = aws_security_group.sagemaker_sg.id  
}
 
output "sagemaker_s3_endpoint" {
    value = aws_vpc_endpoint.sagemaker_s3_endpoint.id
}