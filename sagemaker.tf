data "aws_caller_identity" "current" {}

data "aws_region" "current" {}

# resource "aws_kms_key" "sagemaker_efs_kms_key" {
#   description = "KMS key used to encrypt SageMaker Studio EFS volume"
#   enable_key_rotation = true
# }

# resource "aws_kms_key_policy" "example" {
#   key_id = aws_kms_key.sagemaker_efs_kms_key.id
#   policy = jsonencode({
#     Id = "example"
#     Statement = [
#       {
#         Action = "kms:*"
#         Effect = "Allow"
#         Principal = {
#           AWS = [data.aws_caller_identity.current.account_id]
#         }

#         Resource = "*"
#         Sid      = "Enable IAM User Permissions"
#       },
#     ]
#     Version = "2012-10-17"
#   })
# }



module "sagemaker_domain_execution_role" {
    source = "./modules/iam"
    # kms_arn = aws_kms_key.sagemaker_efs_kms_key.arn
    # execution_role = module.iam.sagemaker_executionrole
}

module "sagmaker_domain_vpc" {
    source = "./modules/vpc"
    # vpc_id = module.vpc.sagemaker_vpc_id
    # subnet_id = module.vpc.sagemaker_subnet_id
    # security_group_ids = module.vpc.sagemaker_securitygrp
}

#     cidr_block = local.vpc.cidr_block
#     private_subnet_cidrs = local.vpc.private_subnet_cidrs
#     azs = local.vpc.availability_zones

resource "aws_sagemaker_domain" "sagemaker_domain" {
    domain_name = var.domain_name
    auth_mode = var.auth_mode
    vpc_id = module.sagmaker_domain_vpc.sagemaker_vpc_id
    subnet_ids = module.sagmaker_domain_vpc.sagemaker_subnet_id

    default_user_settings {
      execution_role = module.sagemaker_domain_execution_role.sagemaker_executionrole
      jupyter_server_app_settings {
        default_resource_spec {
         sagemaker_image_arn = "arn:aws:sagemaker:ap-south-1:394103062818:image/jupyter-server-3"
        }
      }
    #   canvas_app_settings {
    #   time_series_forecasting_settings {
    #     status = "ENABLED"
    #   }
    # }
    } 

    # domain_settings {
      
    # }

  
    # kms_key_id = aws_kms_key.sagemaker_efs_kms_key.arn

    app_network_access_type = var.app_network_access_type

    retention_policy {
      home_efs_file_system = var.home_efs_file_system
    }
}

resource "aws_sagemaker_user_profile" "default_user" {
    domain_id = aws_sagemaker_domain.sagemaker_domain.id
    user_profile_name = var.user_profile_name

    user_settings {
      execution_role = module.sagemaker_domain_execution_role.sagemaker_executionrole
      security_groups = [module.sagmaker_domain_vpc.sagemaker_securitygrp]
    }
}





