variable "domain_name" {
    type = string
    default = "sagemaker-domain"  
}

variable "auth_mode" {
    type = string
    default = "IAM"  
}

variable "aws_region" {
    type = string
    default = "ap-south-1"  
}

variable "user_profile_name" {
    type = string
    default = "defaultuser" 
}

variable "app_network_access_type" {
    type = string
    default = "VpcOnly" 
}

variable "home_efs_file_system" {
    type = string
    default = "Retain"
}

# variable "kms_arn" {
#   description = "kms key to encrypt EFS"
#   type        = string  
# }

# variable "execution_role" {
#   type = string 
# }

# variable "vpc_id" {
#     type = string  
# }

# variable "subnet_id" {
#      type = string 
# }

# variable "security_group_ids" {
#      type = string   
# }